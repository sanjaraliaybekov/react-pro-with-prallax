import React, {Component} from 'react';
import {Col, Row} from "reactstrap";

class FooterEnd extends Component {
    render() {
        return (
            <div className="footer-end">
                <div className="container">
                    <Row>
                        <Col md={3} className="px-0">
                            <p>© 2021 Healthcare - All rights reserved.</p>
                        </Col>
                        <Col md={3} className="offset-md-6 px-0 d-flex justify-content-between">
                            <p>Privacy</p>
                            <p>Security</p>
                            <p>Terms</p>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default FooterEnd;