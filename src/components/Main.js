import React, {Component} from 'react';
import MainTitle from "./MainTitle";
import MainBox from "./MainBox";
import MainTitle2 from "./MainTitle2";
import MainCard from "./MainCard";
import MainContent from "./MainContent";

class Main extends Component {
    state = {
        box: [
            {
                img: "images/heart.svg",
                title: "Cardiology",
                text: "Our cardiologists are skilled at  the diagnosing and treating diseases\n" +
                    "of the cardiovascular system."
            },
            {
                img: "images/opka.svg",
                title: "Pulmonology",
                text: "Our Pulmonologist are skilled at  diagnosing treating diseases of the Pulmonology system."
            },
            {
                img: "images/tabletka.svg",
                title: "Medicine",
                text: "Our medcine doctor are skilled at  diagnosing treating diseases of the  latest medicne system."
            }
        ],
        card:[
            {
                img:"images/cardimg1.svg",
                title:"Dr Amanda Linda",
                text:"Dentist Specilist",
            },
            {
                img:"images/cardimg2.svg",
                title:"Dr. Alisa Rahman",
                text:"Carddiologist Specilist",
            },
            {
                img:"images/cardimg3.svg",
                title:"Dr. Anthony Fauci",
                text:"Neurology Specilist",
            },
            {
                img:"images/cardimg4.svg",
                title:"Dr. Khalid Abbed",
                text:"Cancer Specilist",
            },
        ]
    };

    render() {
        return (
            <div className="main">
                <MainTitle/>
                <MainBox mainBox={this.state.box}/>
                <MainTitle2/>
                <MainCard mainCard={this.state.card}/>
                <MainContent/>
            </div>
        );
    }
}

export default Main;