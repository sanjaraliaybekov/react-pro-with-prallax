import React, {Component} from 'react';
import SecionTitle from "./SecionTitle";
import SectionBox from "./SectionBox";
import SectionContent from "./SectionContent";

class Section extends Component {
    state = {
        box: [
            {
                img: "images/search-Icon.svg",
                title: "Find best doctor’s",
                text: "Find your doctor easily with a minimum of effort. We've kept everything organised for you."
            },
            {
                img: "images/calendar-icon.svg",
                title: "Get Appointment",
                text: "Ask for an appointment of the doctor quickly with almost a single click. We take care of the rest."
            },
            {
                img: "images/online-healthcare-icon.svg",
                title: "Happy Consultations",
                text: "Do consultations and  take the service based on your appointment. Get back to good health"
            }
        ]

    };

    render() {
        return (
            <div className="section">
                <SecionTitle/>
                <SectionBox sectionBox={this.state.box}/>
                <SectionContent/>
            </div>
        );
    }
}

export default Section;