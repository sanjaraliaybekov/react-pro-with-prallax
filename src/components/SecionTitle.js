import React, {Component} from 'react';
import {Col, Row} from "reactstrap";

class SecionTitle extends Component {
    render() {
        return (
            <div className="container">
                <Row>
                    <Col md={12}>
                        <div className="text-center">
                            <h1 className="title">3 Easy Steps and Get Your Solution</h1>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default SecionTitle;