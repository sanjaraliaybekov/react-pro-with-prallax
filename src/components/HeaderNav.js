import React, {Component} from 'react';
import {
    Collapse,
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Button
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Hamburger from 'hamburger-react';
import AOS from 'aos';

class HeaderNav extends Component {
    componentDidMount() {
        AOS.init();
    }

    state = {
        navIsOpen: false,
    };
    navIsOpen = () => {
        this.setState({
                navIsOpen: !this.state.navIsOpen
            }
        )
    };

    render() {
        return (
            <div className="container">
                <Navbar light expand="md" data-aos="fade-down" data-aos-duration="2000">
                    <NavbarBrand href="#"><img src="images/brand-logo.svg" alt=""/></NavbarBrand>
                    <Hamburger toggled={this.state.navIsOpen} toggle={this.navIsOpen}/>
                    <Collapse isOpen={this.state.navIsOpen} navbar>
                        <Nav className="mr-auto" navbar>
                            <NavItem>
                                <NavLink href="#">Home</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#">About</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#">Article</NavLink>
                            </NavItem><NavItem>
                            <NavLink href="#">Services</NavLink>
                        </NavItem>
                            <NavItem>
                                <Button className="btn btn-info">Get Started</Button>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}

export default HeaderNav;