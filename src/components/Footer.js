import React, {Component} from 'react';
import FooterContent from "./FooterContent";
import HrLine from "./HrLine";
import FooterEnd from "./FooterEnd";

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <FooterContent/>
                <HrLine/>
                <FooterEnd/>
            </div>
        );
    }
}

export default Footer;