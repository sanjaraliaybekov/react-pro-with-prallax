import React, {Component} from 'react';
import {Card, CardBody, CardFooter, CardHeader, Col, Row} from "reactstrap";
import {Parallax, Background} from 'react-parallax';
import AOS from 'aos';

class SectionBox extends Component {
    componentDidMount() {
        AOS.init();
    }

    render() {
        const {sectionBox} = this.props;
        return (
            <div className="container">
                <Row>
                    {sectionBox.map((item, index) => (
                        <Col key={index} md={4} data-aos-duration="2000" data-aos="fade-up">
                            <Card className="text-center">
                                <CardHeader>
                                    <div>
                                        <img src={item.img} alt=""/>
                                    </div>
                                </CardHeader>
                                <CardBody>
                                    <h3>{item.title}</h3>
                                </CardBody>
                                <CardFooter>
                                    <h5>{item.text}</h5>
                                </CardFooter>
                            </Card>
                        </Col>
                    ))}
                </Row>
                <Row>
                    <Col md={12} data-aos-duration="2000" data-aos="fade-up">
                        <Parallax
                            bgImage="images/hospital.jpg"
                            renderLayer={percentage => (
                                <div className="absoluteDiv" style={{
                                    position: 'absolute',
                                    background: `linear-gradient(270.26deg, rgba(71, 194, 255, 0.23) 0.21%, rgba(46, 94, 117, 0.79) 97.38%)`,
                                    left: '0',
                                    top: '0',
                                    width: percentage * 500,
                                    height: percentage * 500,
                                }}/>

                            )}>
                            <h3 className="myParallax">SanjarAli Aybekov</h3>
                        </Parallax>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default SectionBox;