import React, {Component} from 'react';
import {AvForm, AvField} from 'availity-reactstrap-validation';
import AOS from 'aos';

class HeaderContent extends Component {
    componentDidMount() {
        AOS.init();
    };

    render() {

        return (
            <div className="container header-content">
                <div className="row">
                    <div className="col-md-6 col-12 title" data-aos="fade-up" data-aos-duration="2000">
                        <h3 className="header-title">Find And Search Your</h3>
                        <h3 className="header-title">Suitable Doctor’s</h3>
                        <p>Join us and take care of yourself and your family with health services that <br/> will make
                            you
                            feel confident and safe in your daily life.</p>
                        <div className="d-flex">
                            <AvForm className="d-flex myForm">
                                <AvField type="text" name="doctor" placeholder="Find your doctor">
                                </AvField>
                                <AvField type="select" name="address">
                                    <option value="New York, USA">New York, USA</option>
                                    <option value="Washington, USA">Washington, USA</option>
                                    <option value="Los Angels, USA">Los Angels, USA</option>
                                </AvField>
                                <button type="submit" className="btn btn-info"><img src="images/search-button.svg"
                                                                                    alt=""/></button>
                            </AvForm>
                        </div>
                    </div>
                    <div className="col-md-6 col-12">
                        <div className="girl position-relative">
                            <img src="images/girl-bg.svg" alt=""/>
                            <img src="images/girl.svg" className="position-absolute" alt="" data-aos="fade-up" data-aos-duration="2000"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default HeaderContent;