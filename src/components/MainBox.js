import React, {Component} from 'react';
import {Card, CardBody, CardFooter, CardHeader, Col, Row} from "reactstrap";
import AOS from 'aos';

class MainBox extends Component {
    componentDidMount() {
        AOS.init();
    }

    render() {
        const {mainBox} = this.props;
        return (
            <div className="container">
                <Row>
                    {mainBox.map((item, index) => (
                        <Col key={index} md={4} data-aos="fade-up" data-aos-duration="2000" data-aos-delay="500">
                            <Card className="text-center hover-box">
                                <CardHeader>
                                    <img src={item.img} alt=""/>
                                </CardHeader>
                                <CardBody>
                                    <h3>{item.title}</h3>
                                </CardBody>
                                <CardFooter>
                                    <h5>{item.text}</h5>
                                </CardFooter>
                            </Card>
                        </Col>
                    ))}

                </Row>
            </div>
        );
    }
}

export default MainBox;