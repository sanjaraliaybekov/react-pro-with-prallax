import React, {Component} from 'react';
import {Card, CardBody, CardFooter, CardHeader, Col, Collapse, Row} from "reactstrap";
import AOS from "aos";

class MainCard extends Component {
    componentDidMount() {
        AOS.init();
    }


    state = {
        isOpen: false
    };
    isOpen = () => {
        this.setState({isOpen: !this.state.isOpen})
    };

    render() {
        const {mainCard} = this.props;
        return (
            <div className="container main-card">
                <Row>
                    {mainCard.map((item, index) => (
                        <Col key={index} md={3} data-aos-duration="2000" data-aos="fade-up">
                            <Card className="text-center">
                                <CardHeader>
                                    <img src={item.img} alt=""/>
                                </CardHeader>
                                <CardBody>
                                    <h3>{item.title}</h3>
                                </CardBody>
                                <CardFooter>
                                    <h5>{item.text}</h5>
                                    <ul>
                                        <li><a href="#"><img src="images/Fb.svg" alt=""/></a></li>
                                        <li><a href="#"><img src="images/Tw.svg" alt=""/></a></li>
                                        <li><a href="#"><img src="images/Ld.svg" alt=""/></a></li>
                                    </ul>
                                </CardFooter>
                            </Card>
                        </Col>
                    ))}
                </Row>
                <Collapse isOpen={this.state.isOpen}>
                    <Row className="mt-4">
                        {mainCard.map((item, index) => (
                            <Col key={index} md={3}>
                                <Card className="text-center">
                                    <CardHeader>
                                        <img src={item.img} alt=""/>
                                    </CardHeader>
                                    <CardBody>
                                        <h3>{item.title}</h3>
                                    </CardBody>
                                    <CardFooter>
                                        <h5>{item.text}</h5>
                                        <ul>
                                            <li><a href="#"><img src="images/Fb.svg" alt=""/></a></li>
                                            <li><a href="#"><img src="images/Tw.svg" alt=""/></a></li>
                                            <li><a href="#"><img src="images/Ld.svg" alt=""/></a></li>
                                        </ul>
                                    </CardFooter>
                                </Card>
                            </Col>
                        ))}
                    </Row>
                </Collapse>
                <Row>
                    <Col md={12}>
                        <button className="btn btn-info" onClick={this.isOpen} type="button">
                            Explore All Doctors
                        </button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default MainCard;