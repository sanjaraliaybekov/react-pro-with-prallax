import React, {Component} from 'react';
import {Col, Row} from "reactstrap";
import {AvForm, AvField} from 'availity-reactstrap-validation'
import AOS from 'aos';

class FooterContent extends Component {
    componentDidMount() {
        AOS.init();
    }

    render() {
        return (
            <div className="container footer-content"  data-aos="fade-up" data-aos-duration="2000">
                <Row>
                    <Col md={4} className="footer-logo px-0">
                        <img src="images/brand-logo.svg" alt=""/>
                        <p>Metairie, 3689 Bassel Street, LA, Louisiana</p>
                        <p>Contact us: 225-788-5489</p>
                        <ul className="logos">
                            <li><a href="/"><img src="images/facebook 1.svg" alt=""/></a></li>
                            <li><a href="/"><img src="images/instagram1.svg" alt=""/></a></li>
                            <li><a href="/"><img src="images/twitter1 .svg" alt=""/></a></li>
                            <li><a href="/"><img src="images/youtube1.svg" alt=""/></a></li>
                        </ul>
                    </Col>
                    <Col md={3} className="offset-md-1 footer-nav px-0">
                        <ul>
                            <li>Category</li>
                            <li><a href="/">Home</a></li>
                            <li><a href="/">About</a></li>
                            <li><a href="/">Services</a></li>
                            <li><a href="/">Reviews</a></li>
                            <li><a href="/">Article</a></li>
                        </ul>
                        <ul>
                            <li>About</li>
                            <li><a href="/">Partners</a></li>
                            <li><a href="/">Careers</a></li>
                            <li><a href="/">Press</a></li>
                            <li><a href="/">Community</a></li>
                        </ul>
                    </Col>
                    <Col md={3} className="offset-md-1 footer-input px-0">
                        <p>Subscribe newsletter</p>
                        <p>Sign up for tips, new destinations, and exclusive promos.</p>
                        <AvForm>
                            <AvField type="email" name="email" placeholder="Enter your email">
                            </AvField>
                            <button type="submit" className="btn btn-info w-100">
                                Submit
                            </button>
                        </AvForm>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default FooterContent;