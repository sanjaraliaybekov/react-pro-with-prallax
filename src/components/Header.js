import React, {Component} from 'react';
import HeaderNav from "./HeaderNav";
import HeaderContent from "./HeaderContent";

class Header extends Component {
    render() {
        return (
            <div className="header">
            <HeaderNav/>
            <HeaderContent/>
            </div>
        );
    }
}

export default Header;