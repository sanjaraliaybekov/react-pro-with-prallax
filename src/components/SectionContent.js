import React, {Component} from 'react';
import AOS from 'aos';

class SectionContent extends Component {
    componentDidMount() {
        AOS.init();
    }

    render() {
        return (
            <div className="container">
                <div className="row" data-aos="fade-up" data-aos-delay="500" data-aos-duration="2000">
                    <div className="person">
                        <img src="images/person-elips.svg" alt=""/>
                        <img src="images/person-bg.svg" alt=""/>
                        <img src="images/person.svg" alt=""/>
                    </div>
                    <div className="content">
                        <h3>Best quality service with our experience
                            doctors</h3>
                        <p>With our top doctors, we are able to provide best medical services above average We have
                            highly experienced doctors, so don't worry They are also very talented in their fields</p>
                        <ul>
                            <li><img src="images/blue-check.svg" alt=""/><p>Search your specialist & Online
                                consultations anywhere</p></li>
                            <li><img src="images/blue-check.svg" alt=""/><p>Consultation our top specialists</p></li>
                            <li><img src="images/blue-check.svg" alt=""/><p>Doctors are available 24/7</p></li>
                        </ul>
                        <button className="btn btn-info" type="button">Explore specialists <img
                            src="images/right-arrow.svg" alt=""/></button>
                    </div>

                </div>
            </div>
        );
    }
}

export default SectionContent;