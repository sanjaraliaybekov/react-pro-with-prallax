import React, {Component} from 'react';
import SectionAfterMainTitle from "./SectionAfterMainTitle";
import SectionAfterMainCard from "./SectionAfterMainCard";
import SectionAfterMainContent from "./SectionAfterMainContent";

class SectionAfterMain extends Component {
    state = {
        card: [
            {
                img: "images/image1.svg",
                img2: "images/quote.svg",
                stars: "images/stars.svg",
                title: "Alan Zara dilan",
                address: "New York, America, USA",
                text: "Telehealth is fueled by digital technologies and DocTime telemedicine app has brought a great revolution in medical services specially an overpopulated country where virtual chamber can create at anywhere thoughout country",
            }, {
                img: "images/image2.svg",
                img2: "images/quote.svg",
                stars: "images/stars.svg",
                title: "Alex Maxwell",
                address: "San Francisco, USA",
                text: "I visited my PCP for abdominal pain. My doctor told me that I need to see Gastroenterologist. Earlier I used to call multiple offices to schedule an appointment that suites my time."
            }, {
                img: "images/image3.svg",
                img2: "images/quote.svg",
                stars: "images/stars.svg",
                title: "Dr. Tasnim zara",
                address: "Los Angeles, USA",
                text: "HealthCare is enlightens my superpower each and every time...!\n" +
                    "I find DocTime a brand-able telemedicine platform with virtual waiting room, video consultation, e-prescription, beautifully planned user interface.\n" +
                    "Thank you :)"
            }
        ],
    };

    render() {
        return (
            <div className="sectionAfterMain">
                <SectionAfterMainTitle/>
                <SectionAfterMainCard card={this.state.card}/>
                <SectionAfterMainContent/>
            </div>
        );
    }
}

export default SectionAfterMain;