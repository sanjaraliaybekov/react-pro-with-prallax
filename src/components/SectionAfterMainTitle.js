import React, {Component} from 'react';
import {Col, Row} from "reactstrap";

class SectionAfterMainTitle extends Component {
    render() {
        return (
            <div>
                <div className="container">
                    <Row>
                        <Col md={12}>
                            <div className="text-center">
                                <h1 className="title">Look our Clients have to say about us</h1>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default SectionAfterMainTitle;