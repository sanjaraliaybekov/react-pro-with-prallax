import React, {Component} from 'react';
import {Col, Row} from "reactstrap";

class MainTitle extends Component {
    render() {
        return (
            <div className="container">
                <Row>
                    <Col md={12}>
                        <div className="text-center">
                            <h1 className="title">Our Service</h1>
                            <p>Our doctors have high qualified skills and are guaranteed <br/> to help you recover from your disease.</p>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default MainTitle;