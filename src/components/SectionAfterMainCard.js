import React, {Component} from 'react';
import {Card, CardBody, CardFooter, CardHeader, Col, Row} from "reactstrap";
import AOS from 'aos';

class SectionAfterMainCard extends Component {
    componentDidMount() {
        AOS.init();
    }

    render() {
        const {card} = this.props;
        return (
            <div className="container SectionAfterMainCard">
                <Row>
                    {card.map((item, index) => (
                        <Col key={index} md={4} data-aos-duration="2000" data-aos="fade-up">
                            <Card>
                                <CardHeader className="text-center">
                                    <img src={item.img} alt=""/>
                                    <div className="bg-dark"><img src={item.img2} alt=""/></div>
                                </CardHeader>
                                <CardBody>
                                    <div>
                                        <h3>{item.title}</h3>
                                        <h4>{item.address}</h4>
                                    </div>
                                    <div>
                                        <img src={item.stars} alt=""/>
                                    </div>
                                </CardBody>
                                <CardFooter>
                                    <h5>{item.text}</h5>
                                </CardFooter>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </div>
        );
    }
}

export default SectionAfterMainCard;