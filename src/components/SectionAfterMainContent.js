import React, {Component} from 'react';
import { Col, Row} from "reactstrap";

class SectionAfterMainContent extends Component {
    render() {
        return (
            <div className="container section-content">
                <Row>
                    <Col md={12}>
                        <div className="d-flex">
                            <div>
                                <h3>
                                    It’s time change your
                                    life today
                                </h3>
                                <button className="btn btn-light">Book an Appoinment </button>
                            </div>
                            <div className="content-img">
                                <img src="images/girl2.png" alt=""/>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default SectionAfterMainContent;