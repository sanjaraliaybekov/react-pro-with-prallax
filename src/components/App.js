import React, {Component} from 'react';
import Header from "./Header";
import Section from "./Section";
import Main from "./Main";
import SectionAfterMain from "./SectionAfterMain";
import Footer from "./Footer";

class App extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Section/>
                <Main/>
                <SectionAfterMain/>
                <Footer/>

            </div>
        );
    }
}

export default App;