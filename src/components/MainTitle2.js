import React, {Component} from 'react';
import {Col, Row} from "reactstrap";

class MainTitle2 extends Component {
    render() {
        return (
            <div className="container">
                <Row>
                    <Col md={12}>
                        <div className="text-center">
                            <h1 className="title">Meet Our Certified Online Doctors</h1>
                            <p>Our online doctors have an average of 15 years experience <br/> and a 98% satisfaction
                                rating, they really set us apart!</p>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default MainTitle2;