import React, {Component} from 'react';

class MainContent extends Component {
    render() {
        return (
            <div className="container main-content">
                <div className="row">
                    <div className="person">
                        <img src="images/iphone.png" alt=""/>
                    </div>
                    <div className="content">
                        <h3>Mobile apps are available <br/>
                            Get HealthCare for free!
                        </h3>
                        <p>Get on-demand access to a doctor on your phone with the free HealthCare mobile app. Download
                            and register on our app for free and feel safe for all your family</p>

                        <button className="btn" type="button">
                            <a href="#">
                                <img src="images/google-play.svg" alt=""/>
                            </a>
                        </button>
                        <button className="btn app-store" type="button">
                            <a href="#">
                                <img src="images/app-store.svg" alt=""/>
                            </a>
                        </button>
                    </div>

                </div>
            </div>
        );
    }
}

export default MainContent;